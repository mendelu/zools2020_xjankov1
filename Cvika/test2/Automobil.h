//
// Created by Terez on 27.04.2020.
//

#ifndef TEST2_AUTOMOBIL_H
#define TEST2_AUTOMOBIL_H


#include <string>

using namespace std;

class Automobil {
protected:
    string m_model;
    int m_najeto;
    int m_puvodniCena;
public:
    Automobil(string model, int najeto, int puvodniCena);

    virtual int getCena() = 0;
    virtual void printInfo() = 0;
};



#endif //TEST2_AUTOMOBIL_H
