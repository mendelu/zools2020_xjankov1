//
// Created by Terez on 08.05.2020.
//

#include "Bylozravec.h"


Bylozravec::Bylozravec(std::string jmeno, int hmotnost, int rychlost) : Zvire(jmeno, hmotnost) {
    m_rychlost = rychlost;
}

bool Bylozravec::utec(Zvire *zvire) {
    if (m_energie - m_hmotnost + m_rychlost > zvire->getEnergie() - zvire->getHmotnost()) {
        return true;
    } else {
        return false;
    }
}

void Bylozravec::printInfo() {
    if (!jeMrtve()) {
        std::cout << "Druh: " << m_jmeno << std::endl;
        std::cout << "Energie: " << m_energie << std::endl;
        std::cout << "Jidlo: " << m_jidlo << std::endl;
        std::cout << "Voda: " << m_voda << std::endl;
        std::cout << "Hmotnost: " << m_hmotnost << std::endl;
        std::cout << "Rychlost: " << m_rychlost << std::endl;
        std::cout << "---------------------" << std::endl;
    } else {
        //if (jeMrtve() || sezer(this)) {
            std::cout << m_jmeno << " je mrtve" << std::endl;
            std::cout << "---------------------" << std::endl;
      //  }
    }
}

bool Bylozravec::jeMasozravec() {
    return false;
}

void Bylozravec::nakrm(int jidlo, int voda, int energie) {
    if (m_jidlo + jidlo <= 100) m_jidlo += jidlo;
    if (m_voda + voda <= 100) m_voda += voda;
    if (m_energie + energie < 100) m_energie += energie;
    if (m_jidlo <= 0)m_jidlo = 0;
    if (m_voda <= 0)m_voda = 0;
    if (m_energie <= 0)m_energie = 0;
}

int Bylozravec::getRychlost() {
    return m_rychlost;
}

bool Bylozravec::sezer(Zvire *korist) {
    return false;
}