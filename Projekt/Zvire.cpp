//
// Created by Terez on 08.05.2020.
//

#include "Zvire.h"
#include "Masozravec.h"
#include "Bylozravec.h"

Zvire::Zvire(std::string jmeno, int hmotnost) {
    m_jmeno = jmeno;
    m_hmotnost = hmotnost;
    m_energie = 100;
    m_jidlo = 100;
    m_voda = 100;

}


std::string Zvire::getZnacka() {
    // M oznacime mrtva zvirata
    if (jeMrtve()) {
        return "M";
    }
        // Z oznacime ziva zvirata
    else {
        return "Z";
    }
}

Zvire::~Zvire(){

}

Zvire *Zvire::getZvire(std::string druh) {
    if (druh == "B") {
        std::string druhBylozravce;
        std::cout << "Vyber si bylozravce Panda/Zajic: " << std::endl;
        std::cin >> druhBylozravce;
        if (druhBylozravce == "Panda") {
            return new Bylozravec("Panda", 100, 15);
        } else if (druhBylozravce == "Zajic") {
            return new Bylozravec("Zajic", 5, 60);
        } else {
            std::cerr << "Tento druh neexistuje, vytvarim zajice." << std::endl;
            return new Bylozravec("Zajic", 5, 60);
        }
    } else if (druh == "M") {
        std::string druhMasozravce;
        std::cout << "Vyber si masozravce Tygr/Lev: " << std::endl;
        std::cin >> druhMasozravce;
        if (druhMasozravce == "Tygr") {
            return new Masozravec("Tygr", 200, 45);
        } else if (druhMasozravce == "Lev") {
            return new Masozravec("Lev", 180, 50);
        } else {
            std::cerr << "Tento druh neexistuje, vytvarim lva." << std::endl;
            return new Masozravec("Lev", 180, 50);
        }
    } else {
        std::cerr << "Toto zvire nelze vytvorit, vytvarim tygra." << std::endl;
        return new Masozravec("Tygr", 200, 45);
    }
}

bool Zvire::jeMrtve() {
    if (m_energie <= 0 || m_jidlo <= 0 || m_voda <= 0 ) {
        return true;
    } else {
        return false;
    }

}

int Zvire::getEnergie() {
    return m_energie;
}

int Zvire::getJidlo() {
    return m_jidlo;
}

int Zvire::getVoda() {
    return m_voda;
}

std::string Zvire::getJmeno() {
    return m_jmeno;
}

int Zvire::getHmotnost() {
    return m_hmotnost;
}

void Zvire::umri() {
    delete (this);
}

