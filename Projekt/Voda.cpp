//
// Created by Terez on 08.05.2020.
//

#include "Voda.h"

Voda::Voda() {
    m_zmenVodu = +40;
    m_zmenEnergii = -35;
    m_zmenJidlo = -30;
};

int Voda::zmenVodu() {
    return m_zmenVodu;

}

int Voda::zmenJidlo() {
    return m_zmenJidlo;
}

int Voda::zmenEnergii() {
    return m_zmenEnergii;
}

std::string Voda::getZnacka() {
    std::string znacka = "";
    if (obsahujeZvire()) {
        for (Zvire *zvire:Pole::getPoleZvirat()) {
            znacka += zvire->getZnacka();
        }
    } else {
        znacka += "V";
    }
    return znacka;
}