//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_VODA_H
#define PROJEKT_VODA_H

#include <iostream>
#include "Pole.h"


class Voda : public Pole {
    int m_zmenEnergii;
    int m_zmenJidlo;
    int m_zmenVodu;
public:
    Voda();

    int zmenJidlo();

    int zmenVodu();

    int zmenEnergii();

    std::string getZnacka();

};


#endif //PROJEKT_VODA_H
