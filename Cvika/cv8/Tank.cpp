//
// Created by Mikulas Muron on 19/04/2020.
//

#include "Tank.h"

Tank::Tank(string jmeno, int sila) {
    m_jmeno = jmeno;
    m_sila_utoku = sila;
    m_zivoty = 100;
}

void Tank::zautoc(Tank *tank, int bonus_terenu) {
    tank->snizZivoty(m_sila_utoku + bonus_terenu);
}

void Tank::snizZivoty(int kolik) {
    m_zivoty = kolik;
    if(m_zivoty < 0){
        m_zivoty = 0;
    }
}

void Tank::printInfo() {
    std::cout << "Tank: "<< m_jmeno << " Zivoty: " << m_zivoty;
}