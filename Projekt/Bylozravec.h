//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_BYLOZRAVEC_H
#define PROJEKT_BYLOZRAVEC_H

#include <iostream>
#include "Zvire.h"
#include "Trava.h"


class Bylozravec : public Zvire {
    int m_rychlost;
public:
    Bylozravec(std::string jmeno, int hmotnost, int rychlost);

    bool utec(Zvire *zvire);

    int getRychlost();

    bool jeMasozravec();

    void nakrm(int jidlo, int voda, int energie);

    void printInfo();

    bool sezer(Zvire *korist);

};

#endif //PROJEKT_BYLOZRAVEC_H
