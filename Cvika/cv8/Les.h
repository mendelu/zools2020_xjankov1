

#ifndef ZOO_CVICENI_LES_H
#define ZOO_CVICENI_LES_H

#include "Teren.h"
#include <iostream>

class Les : public Teren {
    int m_propustnost;
public:
    Les(int skon, int propustnost);
    int getBonus();
    void printInfo();
    std::string getZnacka();

};


#endif //ZOO_CVICENI_LES_H
