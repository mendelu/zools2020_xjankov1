//
// Created by Terez on 27.04.2020.
//

#ifndef TEST2_NAKLADNIAUTOMOBIL_H
#define TEST2_NAKLADNIAUTOMOBIL_H

#include "Automobil.h"
#include <iostream>
#include <string>

using namespace std;

class NakladniAutomobil : public Automobil {
    int m_nosnost;
public:
    NakladniAutomobil(string model, int najeto, int puvodniCena, int nosnost);

    int getCena();
    void printInfo();
};



#endif //TEST2_NAKLADNIAUTOMOBIL_H
