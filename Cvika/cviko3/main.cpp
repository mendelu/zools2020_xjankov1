#include <iostream>
using namespace std;

class Auto{
    float m_cenaKm;
    float m_kolikNajelo;
    float m_vydelek;

public:
    Auto(float cenaKm){
        m_cenaKm = cenaKm;
        m_kolikNajelo = 0;
        m_vydelek = 0;
    }

    Auto(float cenaKm, float kolikNajelo){
        m_cenaKm = cenaKm;
        m_kolikNajelo = kolikNajelo;
        m_vydelek = 0;
    }

    float getVydelek(){
        return m_vydelek;
    }

    float getKolikNajelo(){
        return m_kolikNajelo;
    }
    //muze byt zapsano nekolika variantami
    void registraceAuta(float PujceniKolikNajelo){
        if (checkPujceniKolikNajelo(PujceniKolikNajelo)){
            m_vydelek += vypocitejVydelekPujceni(PujceniKolikNajelo);
            m_kolikNajelo += PujceniKolikNajelo;
        } else {
            cout << "pujceni nebylo zaevydováno" << endl;
        }
    }

private:
    float vypocitejVydelekPujceni(float PujceniKolikNajelo){
        float PujceniVydelek = m_cenaKm*PujceniKolikNajelo;
        return PujceniVydelek;
    }

    void setCenaKm(float CenaKm){
        int maxCenaKm = 1000;
        if ((CenaKm > 0) and (CenaKm < maxCenaKm)){
            m_cenaKm = CenaKm;
        } else {
            m_cenaKm = 0;
            cout << "pozor, cena za km musí být větší, než 0 a mensi nez " << maxCenaKm << endl;
        }
    };

    void setKolikNajeto(float KolikNajeto){
        int maxKolikNajeto = 10000;
        if ((KolikNajeto >= 0) and (KolikNajeto < maxKolikNajeto)){
            m_kolikNajelo = KolikNajeto;
        } else {
            m_kolikNajelo = 0;
            cout << "pozor, najeto musi byt vic, nez 0! a mensi nez" << maxKolikNajeto << endl;
        }
    };

    bool checkPujceniKolikNajelo(float PujceniKolikNajelo){
        int maxPujceniKolikNajelo = 10000;
        if ((PujceniKolikNajelo < 0) and (PujceniKolikNajelo >= maxPujceniKolikNajelo)){
            cout << "pozor, pocet najetych km musi byt vetsi, nez 0 a mensi, nez" << maxPujceniKolikNajelo << endl;
            return false;
        } else {
            return true;
        }
    };
};



int main() {
    Auto * fordMustang = new Auto(10, 5000);
    fordMustang->registraceAuta(555);
    cout << "kolik najelo:" << fordMustang->getKolikNajelo() << endl;
    cout << "vydelek:" << fordMustang->getVydelek() << endl;

    delete fordMustang;
    return 0;
}
