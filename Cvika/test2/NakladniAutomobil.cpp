//
// Created by Terez on 27.04.2020.
//

#include "NakladniAutomobil.h"

NakladniAutomobil::NakladniAutomobil(string model, int najeto, int puvodniCena, int nosnost)
        : Automobil(model, najeto, puvodniCena)
{
    m_nosnost = nosnost;
}

int NakladniAutomobil::getCena() {
    return (1 - (m_najeto / 500000)) * m_puvodniCena;
}

void NakladniAutomobil::printInfo() {
    cout << "Model: " << m_model << endl;
    cout << "Najeto: " << m_najeto << endl;
    cout << "Puvodni cena: " << m_puvodniCena << endl;
    cout << "Nosnost: " << m_nosnost << endl;
}

