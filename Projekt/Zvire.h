//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_ZVIRE_H
#define PROJEKT_ZVIRE_H


#include <iostream>

class Zvire {
protected:
    std::string m_jmeno;
    int m_hmotnost;
    int m_energie;
    int m_jidlo;
    int m_voda;
public:
    Zvire(std::string jmeno, int hmotnost);

    ~Zvire();

    static Zvire *getZvire(std::string druh);

    std::string getJmeno();

    virtual int getHmotnost();

    int getEnergie();

    int getJidlo();

    int getVoda();

    virtual bool jeMasozravec() = 0;

    bool jeMrtve();

    std::string getZnacka();

    virtual void printInfo() = 0;

    virtual void nakrm(int jidlo, int voda, int energie) = 0;

    void umri();

    virtual bool sezer(Zvire *korist) = 0;

    virtual bool utec(Zvire *zvire) = 0;
};


#endif //PROJEKT_ZVIRE_H
