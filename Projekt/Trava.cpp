//
// Created by Terez on 08.05.2020.
//

#include "Trava.h"

Trava::Trava() {
    m_zmenVodu = -35;
    m_zmenEnergii = -40;
    m_zmenJidlo = +15;
}

int Trava::zmenEnergii() {
    return m_zmenEnergii;
}

int Trava::zmenVodu() {
    return m_zmenVodu;
}

int Trava::zmenJidlo() {
    return m_zmenJidlo;
}

std::string Trava::getZnacka() {
    std::string znacka = "";
    if (obsahujeZvire()) {
        for (Zvire *zvire:Pole::getPoleZvirat()) {
            znacka += zvire->getZnacka();
        }
    } else {
        znacka += "T";
    }
    return znacka;
}