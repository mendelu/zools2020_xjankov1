//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_PISEK_H
#define PROJEKT_PISEK_H


#include <iostream>
#include "Pole.h"

class Pisek : public Pole {
    int m_zmenEnergii;
    int m_zmenJidlo;
    int m_zmenVodu;

public:
    Pisek();

    int zmenJidlo();

    int zmenVodu();

    int zmenEnergii();

    std::string getZnacka();


};


#endif //PROJEKT_PISEK_H
