//
// Created by Terez on 08.05.2020.
//

#include "Masozravec.h"

Masozravec::Masozravec(std::string jmeno, int hmotnost, int sila) : Zvire(jmeno, hmotnost) {
    m_sila = sila;
}

int Masozravec::getSila() const {
    return m_sila;
}

void Masozravec::printInfo() {
    if (!jeMrtve()) {
        std::cout << "Druh: " << m_jmeno << std::endl;
        std::cout << "Energie: " << m_energie << std::endl;
        std::cout << "Jidlo: " << m_jidlo << std::endl;
        std::cout << "Voda: " << m_voda << std::endl;
        std::cout << "Hmotnost: " << m_hmotnost << std::endl;
        std::cout << "Sila: " << m_sila << std::endl;
        std::cout << "---------------------" << std::endl;
    } else {
        std::cout << m_jmeno << " je mrtve" << std::endl;
        std::cout << "---------------------" << std::endl;
    }
}

bool Masozravec::sezer(Zvire *korist) {
    std::cout << m_jmeno << " sezral " << korist->getJmeno() << std::endl;
    std::cout << "---------------------" << std::endl;
    if (korist->utec(this)) {
        m_energie -= 25;
        return false;
    } else {
        if (m_jidlo + korist->getJidlo() <= 100) m_jidlo += korist->getJidlo();
        if (m_jidlo + korist->getVoda() <= 100) m_voda += korist->getVoda();
        if (m_jidlo + korist->getEnergie() <= 100) m_energie += korist->getEnergie();
        delete korist;
        return true;
    }
}

bool Masozravec::jeMasozravec() {
    return true;
}

void Masozravec::nakrm(int jidlo, int voda, int energie) {
    if (m_jidlo + jidlo <= 100) m_jidlo += jidlo;
    if (m_voda + voda <= 100) m_voda += voda;
    if (m_energie + energie < 100) m_energie += energie;
}

bool Masozravec::utec(Zvire *zvire) {
    return m_energie - m_hmotnost > zvire->getEnergie() - zvire->getHmotnost();
}
