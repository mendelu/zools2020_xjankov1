//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_MASOZRAVEC_H
#define PROJEKT_MASOZRAVEC_H

#include "Zvire.h"
#include "Pole.h"
#include <iostream>

class Masozravec : public Zvire {
    int m_sila;
public:
    Masozravec(std::string jmeno, int hmotnost, int sila);

    bool sezer(Zvire *korist);

    int getSila() const;

    bool jeMasozravec();

    void printInfo();

    void nakrm(int jidlo, int voda, int energie);

    bool utec(Zvire *zvire);
};


#endif //PROJEKT_MASOZRAVEC_H
