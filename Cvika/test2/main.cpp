#include <iostream>
#include "OsobniAutomobil.h"
#include "NakladniAutomobil.h"
#include "Autobazar.h"

using namespace std;

int main() {
    OsobniAutomobil * a1 = new OsobniAutomobil("Skoda", 500, 10000, false);
    OsobniAutomobil * a2 = new OsobniAutomobil("Honda", 300, 120000, true);
    NakladniAutomobil * a3 = new NakladniAutomobil("Nevim", 400, 123123123, 300);

    Autobazar * autobazar = new Autobazar();
    autobazar->pridejAutomobil(a1);
    autobazar->pridejAutomobil(a2);
    autobazar->pridejAutomobil(a3);
    autobazar->vypisAuta();
    cout << "Majetek celkovy: " << autobazar->getMajetek();
}

