//
// Created by Mikulas Muron on 19/04/2020.
//

#include "Voda.h"

Voda::Voda(int skon, int hloubka): Teren(skon) {
    m_hloubka = hloubka;
}

int Voda::getBonus() {
    return m_hloubka * 2;
}

void Voda::printInfo() {
    std::cout << "Voda: " << getBonus() << std::endl;
}

std::string Voda::getZnacka() {
    return "V";
}