//
// Created by Terez on 27.04.2020.
//

#include "Autobazar.h"

Autobazar::Autobazar() {}

void Autobazar::vypisAuta() {
    for (auto & car : auta) {
        car.printInfo();
    }
}

int Autobazar::getMajetek() {
    int soucet = 0;

    for (auto & car : auta) {
        soucet =+ car.getCena();
    }

    return soucet;
}

void Autobazar::pridejAutomobil(Automobil & automobil) {
    auta.push_back(automobil);
}


