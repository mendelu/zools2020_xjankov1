//
// Created by Mikulas Muron on 19/04/2020.
//

#ifndef ZOO_CVICENI_MAPA_H
#define ZOO_CVICENI_MAPA_H

#include <iostream>
#include <array>
#include "Teren.h"
#include "Voda.h"
#include "Les.h"


using std::cout;
using std::endl;

class Mapa {
    // TODO pouzit NV singleton
    std::array<std::array<Teren*,5>,5> m_mapa;

public:
    Mapa();
    void vykresli();
    void printInfo();
    void umistiTank(int x, int y, Tank *tank);
    void utok(int s_x, int s_y, int d_x, int d_y);
};


#endif //ZOO_CVICENI_MAPA_H
