#include <iostream>
using namespace std;

class Mobil {
private:
    int m_kapacitaBaterie;
    int m_stavBaterie;
    string m_volaciZnak;

public:
    Mobil(int kapacitaBaterie, int stavBaterie, string volaciZnak){
        m_kapacitaBaterie = kapacitaBaterie;
        m_stavBaterie = stavBaterie;
        m_volaciZnak = volaciZnak;
    }

    int getKapacitaBaterie(){
        return m_kapacitaBaterie;
    }
    int getStavBaterie(){
        return m_stavBaterie;
    }
    string getVolaciZnak(){
        return m_volaciZnak;
    }
    int getMozneDobit(){
        return m_kapacitaBaterie - m_stavBaterie;
    }

    void message(int energy){
        m_stavBaterie -= energy;
    }

    void nabit(int energy){
        m_stavBaterie += energy;
    }
};

class EvidenceNabijeni{
private:
    static string m_evidence;
    EvidenceNabijeni(){
    }

public:
    static void zaeviduj(string nabijecka, string mobil, int dobito){
        m_evidence += "Nabijecka " + nabijecka + " nabila mobil " + mobil + " o " + to_string(dobito) + "\n";
    }
    static void vypisEvidenci(){
        cout << m_evidence << endl;
    }
};

string EvidenceNabijeni::m_evidence = "";


class PowerBanka {
private:
    string m_rozhrani;
    int m_mnozstviEnergie;
public:
    PowerBanka(string rozhrani, int mnozstviEnergie){
        m_rozhrani = rozhrani;
        m_mnozstviEnergie = mnozstviEnergie;
    }

    void nabijej(Mobil* mobil){
        int mozneDobit = mobil->getMozneDobit();
        if (mozneDobit >= m_mnozstviEnergie){
            mozneDobit = m_mnozstviEnergie;
        }
        m_mnozstviEnergie -= mozneDobit;
        mobil->nabit(mozneDobit);
        EvidenceNabijeni::zaeviduj(m_rozhrani, mobil->getVolaciZnak(), mozneDobit);

    }
};


int main() {
    Mobil* nokia = new Mobil(100, 30, "nokia");
    Mobil* iphone = new Mobil(1100, 430, "iphone");
    PowerBanka* samsung = new PowerBanka("samsung", 80);

    samsung->nabijej(nokia);
    samsung->nabijej(iphone);

    EvidenceNabijeni::vypisEvidenci();

    delete nokia;
    delete iphone;
    delete samsung;
    return 0;
}