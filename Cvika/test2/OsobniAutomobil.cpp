//
// Created by Terez on 27.04.2020.
//

#include "OsobniAutomobil.h"


OsobniAutomobil::OsobniAutomobil(string model, int najeto, int puvodniCena, bool bourano)
        : Automobil(model, najeto, puvodniCena)
{
    m_bourano = bourano;
}

int OsobniAutomobil::getCena() {
    return (1 - (m_najeto / 300000)) * m_puvodniCena * (1 - 0.5 * m_bourano);
}

void OsobniAutomobil::printInfo() {
    cout << "Model: " << m_model << endl;
    cout << "Najeto: " << m_najeto << endl;
    cout << "Puvodni cena: " << m_puvodniCena << endl;
    cout << "Bourano: " << m_bourano << endl;
}

