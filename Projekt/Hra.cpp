//
// Created by Terez on 21.05.2020.
//

#include "Hra.h"

void Hra::spustiHru() {
    Mapa *mapa = Mapa::getMapa();
    mapa->vypisMapu();
    std::string druh;
    int p = 0;
    std::cout << "Zvol si pocet zvirat: ";
    std::cin >> p;

    for (int i = 0; i < p; i++) {
        std::cout << "Zvol si druh B/M: ";
        std::cin >> druh;

        Zvire *zvire = nullptr;

        zvire = Zvire::getZvire(druh);
        mapa->ulozNaPozici(0, 0, zvire);
    };
    int k = 0;
    std::cout << "Zvol si pocet kol: ";
    std::cin >> k;

    for (int i = 0; i < k; i++) {
        mapa->vypisMapu();
        mapa->pohybZvirat();
        mapa->akceZvirat();
    };
    mapa->vypisMapu();


}

