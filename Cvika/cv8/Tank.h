//
// Created by Mikulas Muron on 19/04/2020.
//

#ifndef ZOO_CVICENI_TANK_H
#define ZOO_CVICENI_TANK_H
#include <iostream>
using std::string;

class Tank {
    string m_jmeno;
    int m_zivoty;
    int m_sila_utoku;
public:
    Tank(string jmeno, int sila);
    void zautoc(Tank* tank, int bonus_terenu);
    void snizZivoty(int kolik);
    void printInfo();
};


#endif //ZOO_CVICENI_TANK_H
