#include <iostream>
using namespace std;

class Zidle{
public:
    string m_material;
    int m_nosnost;

    int getNosnost(){
        return m_nosnost;
    }

    void setNosnost(int nova_nosnost){
        m_nosnost = nova_nosnost;
    }
    string getMaterial(){
        return m_material;
    }
    void setMaterial(string novy_material){
        m_material = novy_material;
    }
    void printInfo(){
        cout<<"jsem z: "<<m_material<<endl;
        cout<<"mam nosnost"<<m_nosnost<<endl;
    }
};
class Zidle2{
public:
    int m_nosnost2;
    string m_material2;

    int getNosnost2(){
        return m_nosnost2;
    }

    void setNosnost2(int nova_nosnost2){
        m_nosnost2 = nova_nosnost2;
    }
};

int main() {
    int cislo;
    Zidle* z1 = new Zidle();
    Zidle* z2 = new Zidle();
    z1 -> setNosnost(10);
    z1 -> printInfo();
    z2 -> setMaterial("drevo");
    z2 -> printInfo();

    return 0;
}