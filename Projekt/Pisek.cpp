//
// Created by Terez on 08.05.2020.
//

#include "Pisek.h"

Pisek::Pisek() {
    m_zmenVodu = -50;
    m_zmenEnergii = +35;
    m_zmenJidlo = -45;
}

int Pisek::zmenEnergii() {
    return m_zmenEnergii;
}

int Pisek::zmenJidlo() {
    return m_zmenJidlo;

}

int Pisek::zmenVodu() {
    return m_zmenVodu;
}

std::string Pisek::getZnacka() {
    std::string znacka = "";
    if (obsahujeZvire()) {
        for (Zvire *zvire:Pole::getPoleZvirat()) {
            znacka += zvire->getZnacka();
        }
    } else {
        znacka += "P";
    }
    return znacka;
}
