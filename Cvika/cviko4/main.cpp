#include <iostream>
using namespace std;

class KomunikaceEvidence{
    static string m_log;
    KomunikaceEvidence(){
    }

public:
    static void logUdalost(string from, string kam, string co){
        m_log += from + "->" + kam + ": " + co + "\n";
    }
    static void printUdalost(){
        cout << "Log:"<< endl;
        cout << m_log << endl;
    }
};

string KomunikaceEvidence::m_log = "";

class Zvire{
    string m_jmeno;
    int m_kalorie;
public:
    Zvire(string jmeno, int kalorie){
        setJmeno(jmeno);
        setKalorie(kalorie);
    }

    int getKalorie(){
        return m_kalorie;
    }

    string getJmeno(){
        return m_jmeno;
    }

private:
    void setJmeno(string jmeno){
        if(jmeno != ""){
            m_jmeno = jmeno;
        } else {
            cout << "jmeno neni zadano" << endl;
            m_jmeno = "Prazdne jmeno";
        }
    }

    void setKalorie(int kalorie){
        if(kalorie >= 1){
            m_kalorie = kalorie;
        } else {
            cout << "kalorie musí být mensi, nez 1" << endl;
            m_kalorie = 1;
        }
    }
};

class Drak{
    int m_jistKalorie;
public:
    Drak(){
        m_jistKalorie = 0;
    }
    void jistZvire(int kalorie){
        if(kalorie >= 1){
            m_jistKalorie += kalorie;
            KomunikaceEvidence::logUdalost( "-", "drak", "kalorie" );
        } else {
            cout << "kalorie musi byt mensi, nez 1"<<endl;
        }
    }

    void jistZvire(Zvire* zvire){
        if(zvire != nullptr){
            KomunikaceEvidence::logUdalost("zvire", "Drak", "kalorie");
            m_jistKalorie += zvire->getKalorie();
        } else {
            cout << "kalorie musi byt mensi, nez 1"<<endl;
        }
    }

    void printInfo(){
        cout<<"sneyene kalorie " << m_jistKalorie << endl;
    }
};

int main() {
    Zvire* veverka = new Zvire("tereza", 20);
    Zvire* medved = new Zvire("Adam", 50);

    Drak* karel = new Drak();
    karel->jistZvire( veverka->getKalorie() );
    karel->jistZvire(medved);
    karel->printInfo();

    KomunikaceEvidence::printUdalost();

    delete veverka;
    delete medved;
    delete karel;
    return 0;
}
