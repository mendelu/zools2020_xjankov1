//
// Created by Terez on 08.05.2020.
//

#include "Pole.h"

void Pole::pridejZvire(Zvire *zvire) {
    m_zvirata.push_back(zvire);

}

void Pole::setPoleVOkoli(std::vector<Pole *> poleVOkoli) {
    m_poleVOkoli = poleVOkoli;
}

void Pole::interakceZvirete() {
    for (Zvire *zvire: m_zvirata) {
        for (Zvire *zvireKSezrani: m_zvirata) {
// zvire->snez(zvireKSezrani);
        }
    }
}

Pole *Pole::getNahodnePoleZOkoli() {
    // nahodne vyberu index
    int nahodny_index = rand() % m_poleVOkoli.size();
    // vratim
    return m_poleVOkoli[nahodny_index];
}

void Pole::posunZvirete(std::vector<Zvire *> *jizPosunutaZvirata) {
    for (Zvire *zvire: m_zvirata) {
        // nehybeme s mrtvyma zviratama
        if (!zvire->jeMrtve()) {
            // nehybeme uz s jednou posunutyma zviratama
            if (std::find(jizPosunutaZvirata->begin(), jizPosunutaZvirata->end(), zvire) == jizPosunutaZvirata->end()) {
                // posunu zvire na nahodne policko z okoli
                getNahodnePoleZOkoli()->pridejZvire(zvire);
                jizPosunutaZvirata->push_back(zvire);
                // smazu zvire z aktualniho policka
                m_zvirata.erase(std::find(m_zvirata.begin(), m_zvirata.end(), zvire));

            }
        }
    }
    nakrmZvire();
}

std::string Pole::getZnacka() {
    std::string znacka = " ";
    if (obsahujeZvire()) {
        for (Zvire *zvire: m_zvirata) {
            znacka += zvire->getZnacka();
        }
    } else {
        znacka += "- ";
    }
    return znacka;
}


bool Pole::obsahujeZvire() {
    return m_zvirata.size() > 0;
}

std::vector<Zvire *> &Pole::getPoleZvirat() {
    return m_zvirata;
}

void Pole::nakrmZvire() {
    if (m_zvirata.size() > 0) {
        if (m_zvirata.size() > 1) {
            for (int i = 0; i < m_zvirata.size(); i++) {
                if (m_zvirata.at(i)->jeMasozravec()) {

                } else {

                    m_zvirata.at(i)->nakrm(zmenJidlo(), zmenVodu(), zmenEnergii());
                }
            }
        } else {
            if (m_zvirata.at(0)->jeMasozravec()) {

            } else {
                m_zvirata.at(0)->nakrm(zmenJidlo(), zmenVodu(), zmenEnergii());
            }

        }
    }
}