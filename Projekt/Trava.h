//
// Created by Terez on 08.05.2020.
//

#ifndef PROJEKT_TRAVA_H
#define PROJEKT_TRAVA_H

#include "Pole.h"

class Trava : public Pole {
    int m_zmenEnergii;
    int m_zmenJidlo;
    int m_zmenVodu;

public:

    Trava();


    int zmenJidlo();

    int zmenVodu();

    int zmenEnergii();

    std::string getZnacka();

};


#endif //PROJEKT_TRAVA_H
