
#ifndef ZOO_CVICENI_TEREN_H
#define ZOO_CVICENI_TEREN_H

#include <string>
#include "Tank.h"

class Teren {
protected:
    int m_skon;
    Tank* m_tank;
public:
    Teren(int skon);
    void setTank(Tank* tank);
    Tank* getTank();
    bool obsahujeTank();
    virtual int getBonus() = 0;
    virtual void printInfo() = 0;
    virtual std::string getZnacka() = 0;
};


#endif //ZOO_CVICENI_TEREN_H
