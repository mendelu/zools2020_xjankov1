//
// Created by Terez on 27.04.2020.
//

#ifndef TEST2_OSOBNIAUTOMOBIL_H
#define TEST2_OSOBNIAUTOMOBIL_H


#include "Automobil.h"
#include <iostream>
#include <string>

using namespace std;

class OsobniAutomobil : public Automobil {
    bool m_bourano;
public:
    OsobniAutomobil(string model, int najeto, int puvodniCena, bool bourano);

    int getCena();
    void printInfo();
};


#endif //TEST2_OSOBNIAUTOMOBIL_H
