//
// Created by Terez on 27.04.2020.
//

#ifndef TEST2_AUTOBAZAR_H
#define TEST2_AUTOBAZAR_H

#include <vector>
#include <string>
#include "Automobil.h"

using namespace std;

class Autobazar {
    vector<Automobil> auta;
public:
    Autobazar();

    void vypisAuta();
    int getMajetek();
    void pridejAutomobil(Automobil * automobil);
    void stocKilometry(Automobil * automobil);
};


#endif //TEST2_AUTOBAZAR_H
